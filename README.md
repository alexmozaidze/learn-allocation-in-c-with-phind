# What is this repository about?

This repository contains code that was written while I was learning how to make a vector in C (like the `std::vector` one).

# Why?

I asked Phind the following question: "What projects can I make to learn allocation in C?"  
And so I got the following response:

> Based on the context provided, here are some project ideas to help you learn memory allocation in C:
> 
> 1. **Array Resizing**: Implement a dynamic array that can be resized during runtime. This will help you understand how to use memory allocation functions like `malloc()`, `calloc()`, `realloc()`, and `free()` to manage the memory used by the array [Source 2](https://www.geeksforgeeks.org/dynamic-memory-allocation-in-c-using-malloc-calloc-free-and-realloc/).
> 
> Pros:
> - Gain a deep understanding of dynamic memory allocation.
> - Learn how to manage memory efficiently.
> 
> Cons:
> - May be limited in scope compared to other project ideas.
> 
> 2. **Basic Database System**: Create a simple database system that allows users to add, modify, view, search, and delete data using file handling and dynamic memory allocation. This will help you understand how to allocate and manage memory for different types of data structures and operations [Source 0](https://www.interviewbit.com/blog/c-projects/).
> 
> Pros:
> - Learn about file handling and memory management.
> - Gain experience working with real-world data structures.
> 
> Cons:
> - May require additional knowledge about database systems.
> 
> 3. **Text Editor**: Build a text editor that supports basic operations like opening, editing, saving, and closing files. This project will require you to work with dynamic memory allocation to handle user input and manage the text buffer efficiently [Source 4](https://www.toptal.com/c/the-ultimate-list-of-resources-to-learn-c-and-c-plus-plus).
> 
> Pros:
> - Learn how to handle user input and manage memory.
> - Gain experience working with file I/O and memory allocation.
> 
> Cons:
> - May require additional knowledge about user interfaces and text processing.
> 
> 4. **Memory Allocator**: Implement a custom memory allocator that uses `malloc()`, `calloc()`, `realloc()`, and `free()` to manage memory. This project will help you understand the internal workings of memory allocation and how to optimize memory usage [Source 2](https://www.geeksforgeeks.org/dynamic-memory-allocation-in-c-using-malloc-calloc-free-and-realloc/).
> 
> Pros:
> - Gain a deep understanding of memory allocation and management.
> - Learn how to optimize memory usage.
> 
> Cons:
> - May be challenging for beginners.
> 
> When working on these projects, consider using resources like [LearnVern](https://www.learnvern.com/) for beginner-friendly courses, [C++ Annotations](http://www.icce.rug.nl/documents/cplusplus/) for transitioning from C to C++, and books like [C++ FAQs (2nd Edition) by Marshall Cline](https://www.amazon.com/C-FAQs-Second-Marshall-Cline/dp/0201309831) to dive deeper into specific topics.

# Building

Before running any of the commands, ensure that you're in the project's root directory.

Initializing the project is done by running

``` sh
cmake -B build -S .
```

Note that you only need to run it once

And to build the executables

``` sh
cmake --build build
```

# Projects

Most projects solve a very specific problem in a very limited way. I'm not trying to reinvent the wheel here.

## dynamic-array

This project defines and plays around with a vector.

I finished up the project in approximately 8 hours.
