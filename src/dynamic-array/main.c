#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>

#define THE_ANSWER 42
#define FUNY_NUMBER 69
#define BEST_NUMBER 26

typedef struct Vec {
    int capacity;
    int length;
    int * ptr;
} Vec;

/// Creates the vector
///
/// It always allocates and zero-initializes 1 element
bool vec_new(Vec * vec, int capacity) {
    if (capacity <= 0) return false;

    int * ptr = calloc(capacity, sizeof(int));
    if (ptr == NULL) return false;

    vec->length = capacity;
    vec->capacity = capacity;
    vec->ptr = ptr;

    return true;
}

/// Pushes an element to the vector
bool vec_push(Vec * vec, int element) {
    if (vec->length == vec->capacity) {
        int *ptr = realloc(vec->ptr, (sizeof(int) * vec->capacity) + sizeof(int));
        if (ptr == NULL) return false;

        vec->ptr = ptr;
        vec->capacity++;
    }

    vec->ptr[vec->length] = element;
    vec->length++;

    return true;
}

/// Prints the vector
void vec_print(Vec * vec) {
    printf("[");
    int i;
    for (i = 0; i < vec->length - 1; i++) {
        printf("%i, ", vec->ptr[i]);
    }
    printf("%i]\n", vec->ptr[i]);
}

/// Prints vector's properties, then prints vector itself
void vec_print_verbose(Vec * vec) {
    printf("length: %i\ncapacity: %i\narray: ", vec->length, vec->capacity);
    vec_print(vec);
}

/// Deinitializes the vector
void vec_deinit(Vec * vec) {
    free(vec->ptr);
    vec->length = 0;
    vec->capacity = 0;
}

int main(void) {
    printf("This program cretes a vector (dynamicaly-sized array) and pushes elements to it\n");
    printf("There is no user interaction, you HAVE to edit the source code to change things up\n");
    printf("The error handling is not conventional (nor is it good), but it was not the focus of this project\n");
    printf("\n");

    Vec my_vec;
    if (!vec_new(&my_vec, 1)) return EXIT_FAILURE;
    my_vec.ptr[0] = THE_ANSWER;

    vec_print_verbose(&my_vec);
    printf("\n");

    if (!vec_push(&my_vec, BEST_NUMBER)) return EXIT_FAILURE;

    vec_print_verbose(&my_vec);
    printf("\n");

    if (!vec_push(&my_vec, FUNY_NUMBER)) return EXIT_FAILURE;

    vec_print_verbose(&my_vec);
    printf("\n");

    vec_deinit(&my_vec);

    return EXIT_SUCCESS;
}
